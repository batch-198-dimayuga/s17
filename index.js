/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function userDetails(){
    let fullName = prompt("Enter your fullname: ");
    let age = prompt("Enter your age: ")
    let location = prompt("Enter your location: ");

    console.log('Hi ' + fullName);
    console.log(fullName + "'s age is " + age);
    console.log(fullName + ' is located at ' + location);
}
userDetails();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function faveBands(){
    let bands = ["My Chemical Romance", "Panic! At The Disco", "Justin Bieber", "Harry Styles", "Charlie Puth"];
    console.log(bands);
};    
faveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function faveMovies(){
    console.log("1. Lord of the Rings : The Return of the King");
    console.log("Tomatometer for The Return of the King: 67%");
    console.log("2. Lord of the Rings : The Fellowship of the Ring");
    console.log("Tomatometer for The Fellowship of the Rung: 91%");
    console.log("3. Lord of the Rings : The Two Towers");
    console.log("Tomatometer for The Two Towers: 95%");
    console.log("4. Kimetsu no Yaiba: Mugen Train");
    console.log("Tomatometer for Kimetsu no Yaiba: 98%");
    console.log("5. Spirited Away");
    console.log("Tomatometer for Spirited Away: 97%");
}
faveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();


// console.log(friend1);
// console.log(friend2);